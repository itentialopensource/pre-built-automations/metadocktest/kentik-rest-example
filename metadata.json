{
  "name": "Kentik - REST - Example",
  "webName": "Kentik - Example Use Case Bundle",
  "type": "Example Project",
  "vendor": "Kentik",
  "product": "Kentik Network Observability Platform",
  "method": "REST",
  "osVersions": [
    ""
  ],
  "apiVersions": [
    ""
  ],
  "iapVersions": [
    "2023.1"
  ],
  "domains": [
    "Observability"
  ],
  "tags": [
    "Notifications & Ticket Management",
    "Monitoring & Troubleshooting"
  ],
  "useCases": [
    "Add Device to Monitoring",
    "Automated network data gathering for diagnostics"
  ],
  "deprecated": {
    "isDeprecated": false
  },
  "brokerSince": "",
  "documentation": {
    "storeLink": "",
    "repoLink": "https://gitlab.com/itentialopensource/pre-built-automations/kentik-rest-example",
    "npmLink": "https://www.npmjs.com/package/@itentialopensource/kentik-rest-example",
    "docLink": "",
    "demoLinks": [
      {
        "title": "Itential Kentik Partnership Overview",
        "link": "https://www.itential.com/tech-partners/kentik/"
      }
    ],
    "trainingLinks": [],
    "faqLink": "",
    "contributeLink": "",
    "issueLink": "",
    "webLink": "https://www.itential.com/automations/kentik-example-use-case-bundle/",
    "vendorLink": "https://www.kentik.com/",
    "productLink": "https://www.kentik.com/product/kentik-platform/",
    "apiLinks": [
      {
        "title": "Kentik Notification Channel Management Overview",
        "link": "https://kb.kentik.com/v4/Cb24.htm#Cb24-Manage_Notification_Channels",
        "public": true
      },
      {
        "title": "Kentik API Reference",
        "link": "https://kb.kentik.com/v0/Ab09.htm",
        "public": true
      },
      {
        "title": "Kentik Alert Policies Management Overview",
        "link": "https://kb.kentik.com/v4/Ga08.htm#Ga08-Manage_Alert_Policies",
        "public": true
      },
      {
        "title": "Microsoft Teams Creating Incoming Webhooks Overview",
        "link": "https://learn.microsoft.com/en-us/microsoftteams/platform/webhooks-and-connectors/how-to/add-incoming-webhook?tabs=dotnet",
        "public": true
      },
      {
        "title": "Kentik General Dimentions Overview",
        "link": "https://kb.kentik.com/v0/Ia04.htm",
        "public": true
      },
      {
        "title": "Kentik Custom Webhook Templating Reference",
        "link": "https://github.com/kentik/custom-notification-templates/blob/main/docs/TEMPLATING_REFERENCE.md",
        "public": true
      }
    ]
  },
  "assets": [
    {
      "name": "AWS Blocked Traffic - Kentik - Example",
      "webName": "AWS Blocked Traffic - Kentik - Example",
      "assetType": "Workflow",
      "overview": "This is an end-to-end automation example where an alert is triggered within a Kentik policy due to a threshold being exceeded. In this example Kentik uses an integration to call IAP's <a href='https://docs.itential.com/docs/triggers-2023-1#api-endpoint-triggers' target='_blank'>northbound API trigger</a>. Once the trigger is executed it invokes an automation that then validates the traffic can be accepted and interacts with change management (ServiceNow), AWS EC2, and a notification platform (MS Teams).\n\nThis automation example can be installed and reviewed for ideas on how to incorporate Kentik into a closed loop alert remediation scenario with IAP. If interested in fully running this automation, see `Configuring Dependencies` below for setting up required dependencies.\n\n### Configuring Dependencies\n\n#### AWS\n\nA <a href='https://docs.aws.amazon.com/vpc/latest/userguide/vpc-security-groups.html' target='_blank'>Security Group in AWS</a> grants access of a hosted server to given source IP. This automation is able to update a given Security Group to have source IP after alert is sent by Kentik to IAP in the Kentik <a href='https://kb.kentik.com/v0/Ia04.htm#Ia04-AWS_Dimensions' target='_blank'>AWS Dimension field kt_aws_dst_sg</a>.\n\n#### Microsoft Teams\n\nThis IAP automation sends formatted messages over Microsoft Teams with links to the IAP job run, a ServiceNow Change Request created for an alarm event, and the Kentik flow data. Three channels are used in this automation that each require creating an Incoming Webhook and are used for the following events:\n\n1. `Policy All Events`: receives messages any time an alert is sent from Kentik to IAP and starts a job in IAP.\n2. `Policy Automated Authorizations`: receives messages when a source IP detected by Kentik sending web traffic to a given server is added to Security Group in AWS to grant access to that server since the source IP is found in a white-list.\n3. `Policy Automated Failures`: received messages when a source IP fails to be added to Security Group in AWS. This can occur if a failure happens when performing that operation or the source IP is already found in the Security Group.\n\nFollow the Microsoft Teams documentation linked for <a href='https://learn.microsoft.com/en-us/microsoftteams/platform/webhooks-and-connectors/how-to/add-incoming-webhook?tabs=dotnet' target='_blank'>creating Incoming Webhooks</a> for each of the three channels described above.\n\n#### Kentik\n\nThe AWS Blocked Traffic Event automation is started by Kentik sending a request to IAP upon a critical threshold met in traffic to a web server observed by Kentik. In order to use this automation a Notification Channel of type Custom Webhook must be set up in Kentik. The IAP URL and API endpoint for the AWS Blocked Traffic Event automation is provided in the configuration of the Custom Webhook in Kentik. A Go template as seen below is used to map the data sent from Kentik to a format the IAP automation can parse to perform a closed loop remediation of either enabling access to client sending traffic to web server observed by Kentik or continuing to block access if the IP is not white-listed.\n\n```python\n{\n  {{- . | toJSON | explodeJSONKeys -}},\n  {{- if .IsSingleEvent  -}}\n    {{- with .Event -}}\n      {{- . | toJSON | explodeJSONKeys -}},\n      {{- .Details.General.ToMap | toJSON | explodeJSONKeys -}},\n      \"iapUrl\": \"<iap_url>\",\n      \"serviceNowUrl\": \"<service_now_url>\",\n      \"allPolicyEventsWebhookUrl\": \"<webhook_url_1>\",\n      \"policyAutomatedFailureWebhookUrl\": \"<webhook_url_2>\",\n      \"policyAutomatedAuthorizationWebhookUrl\": \"<webhook_url_3>\",\n      \"serviceNowAdapter\": \"<service_now_adapter>\",\n      \"awsEC2Adapter\": \"<aws_ec2_adapter>\",\n      \"msTeamsAdapter\": \"<ms_teams_adapter>\",\n      \"Dimensions\": {{- (.Details.WithTag \"dimension\").ToMap | toJSON -}},\n      \"Links\": {{- (.Details.WithTag \"url\") | toJSON -}}\n    {{- end -}}\n  {{- else -}}\n    {{- with index .Events 0 -}}\n      {{- . | toJSON | explodeJSONKeys -}},\n      {{- .Details.General.ToMap | toJSON | explodeJSONKeys -}},\n      \"iapUrl\": \"<iap_url>\",\n      \"serviceNowUrl\": \"<service_now_url>\",\n      \"allPolicyEventsWebhookUrl\": \"<webhook_url_1>\",\n      \"policyAutomatedFailureWebhookUrl\": \"<webhook_url_2>\",\n      \"policyAutomatedAuthorizationWebhookUrl\": \"<webhook_url_3>\",\n      \"serviceNowAdapter\": \"<service_now_adapter>\",\n      \"awsEC2Adapter\": \"<aws_ec2_adapter>\",\n      \"msTeamsAdapter\": \"<ms_teams_adapter>\",\n      \"Dimensions\": {{- (.Details.WithTag \"dimension\").ToMap | toJSON -}},\n      \"Links\": {{- (.Details.WithTag \"url\") | toJSON -}}\n    {{- end -}}\n  {{- end -}}\n}\n```\n\nOnce the Custom Webhook is created with the above Go template as well as URL of the IAP API endpoint, a <a href='https://kb.kentik.com/v4/Ga08.htm#Ga08-Manage_Alert_Policies' target='_blank'>Kentik Alert Policy</a> must be configured to send a request to IAP upon a threshold being reached using Custom Webhook created above. This automation was tested using a Critical threshold in the Alert Policy, but other threshold levels could be used.\n\n#### IP Whitelist\n\nA <a href='https://docs.itential.com/docs/newvariable-task-2023-1?highlight=new%20job%20variable' target='_blank'>newVariable task</a> at the start of the workflow `AWS Blocked Traffic Event - Kentik - Example` is used to create an IP whitelist for allowed source IPs to reach the server in AWS that Kentik is observing.\n\n### Running Automation and Resetting State\n\nIn order for Kentik to send a request to IAP the client source IP must not exist in the corresponding security group for the server in AWS. The `AWS Blocked Traffic - Reset - Kentik - Example` Operations Manager automation can delete a given source IP from a given security group.\n\nThere is a delay between when a source IP not in the AWS security group attempts to reach the server on AWS and when the Kentik request due to alarm threshold met is sent to IAP. Look for messages in Microsoft Teams when the request is sent to IAP from Kentik.",
      "iapVersions": [
        "2023.1"
      ],
      "exampleInput": "{\n  \"iapUrl\": \"https://iap-dev:443\",\n  \"serviceNowUrl\": \"https://service-now.com\",\n  \"allPolicyEventsWebhookUrl\": \"webhookurl1\",\n  \"policyAutomatedFailureWebhookUrl\": \"webhookurl2\",\n  \"policyAutomatedAuthorizationWebhookUrl\": \"webhookurl3\",\n  \"serviceNowAdapter\": \"ServiceNow\",\n  \"awsEC2Adapter\": \"AWS\",\n  \"msTeamsAdapter\": \"MS Teams\",\n  \"AlarmPolicyName\": \"US Policy Rejected Web Server Traffic\",\n  \"Dimensions\": {\n    \"AS_src\": \"1000\",\n    \"IP_dst\": \"11.11.11.11\",\n    \"IP_src\": \"10.10.10.10\",\n    \"kt_aws_dst_sg\": \"aws_security_group\"\n  },\n  \"Links\": [\n    {\n      \"Name\": \"DashboardAlarmURL\",\n      \"Label\": \"Open Dashboard\",\n      \"Value\": \"https://portal.kentik.com/v4/library/dashboards/10000\"\n    },\n    {\n      \"Name\": \"DetailsAlarmURL\",\n      \"Label\": \"Open Details\",\n      \"Value\": \"https://portal.kentik.com/v4/alerting/a215729224\"\n    }\n  ]\n}",
      "exampleOutput": ""
    },
    {
      "name": "AWS Blocked Traffic - Reset - Kentik - Example",
      "webName": "AWS Blocked Traffic - Reset - Kentik - Example",
      "assetType": "Workflow",
      "overview": "Workflow that is used to remove source IP from security group in AWS to reset data for AWS Blocked Traffic - Kentik - Example automation",
      "iapVersions": [
        "2023.1"
      ],
      "exampleInput": "{\n    \"sourceIP\": \"10.10.10.10\",\n    \"awsEC2Adapter\": \"AWS\",\n    \"awsSecurityGroup\": \"security_group\"\n  }",
      "exampleOutput": ""
    },
    {
      "name": "Device Onboarding - Kentik - Example",
      "webName": "Device Onboarding - Kentik - Example",
      "assetType": "Workflow",
      "overview": "This automation example can be installed and reviewed for ideas on how to query a Cisco IOS device from NetBox that has already been onboarded to Itential Automation Gateway (IAG) and then onboards the device into Kentik. Once onboarded into Kentik, the device is configured via IAG to send flow data to Kentik. Finally, the automation validates that Kentik has received flow data from the device.\n\nIAP updates NetBox with the Kentik device ID and keeps Change Management up to date integrating with ServiceNow and MS Teams for notifications throughout the automation. If interested in fully running this automation, see `Configuring Dependencies` below for setting up required dependencies.\n\n### Configuring Dependencies\n\n#### NetBox\n\nA single Cisco IOS device needs to be set in NetBox that has a name, a site name, and local config context data. See example object with these fields provided below.\n\n```json\n{\n  \"name\": \"ATLSWITCH01\",\n  \"site\": {\n    \"name\": \"ATL HQ\"\n  },\n  \"config_context\": {\n    \"ipAddress\": \"1.2.3.4\",\n    \"sampleRate\": 1,\n    \"snmpV3Conf\": {\n      \"userName\": \"username\",\n      \"privacyProtocol\": \"AES\",\n      \"privacyPassphrase\": \"passphrase\",\n      \"authenticationProtocol\": \"SHA\",\n      \"authenticationPassphrase\": \"password\"\n    },\n    \"description\": \"IOS Device\"\n  }\n}\n```\n\n#### Microsoft Teams\n\nThis IAP automation sends formatted messages over Microsoft Teams with links to the IAP job run, a ServiceNow Change Request created for the device onboarding event, and the Kentik flow data. One Microsoft Teams channel called `Device Onboarding` is used in this automation that requires creating an Incoming Webhook.\n\nFollow the Microsoft Teams documentation linked for <a href='https://learn.microsoft.com/en-us/microsoftteams/platform/webhooks-and-connectors/how-to/add-incoming-webhook?tabs=dotnet' target='_blank'>creating Incoming Webhooks</a> for setting up the webhook for that channel.\n\n#### Itential Automation Gateway\n\nA Cisco IOS device must be onboarded to Itential Automation Gateway (IAG) as an Ansible inventory device. See example properties in JSON object below for this:\n\n```json\n{\n  \"ansible_network_os\": \"ios\",\n  \"host_key_checking\": 0,\n  \"ansible_port\": 22,\n  \"ansible_user\": \"username\",\n  \"ansible_password\": \"password\",\n  \"ansible_host\": \"10.10.10.10\",\n  \"ansible_connection\": \"network_cli\"\n}\n```\n",
      "iapVersions": [
        "2023.1"
      ],
      "exampleInput": "{\n  \"formData\": {\n    \"iapUrl\": \"https://iap-dev:443\",\n    \"deviceOnboardingWebhookUrl\": \"webhookURL1\",\n    \"serviceNowUrl\": \"https://service-now-dev.com\",\n    \"deviceName\": \"ios_device\",\n    \"serviceNowAdapter\": \"ServiceNow\",\n    \"netBoxAdapter\": \"NetboxV3\",\n    \"msTeamsAdapter\": \"MS Teams\",\n    \"kentikAdapter\": \"Kentik v5\"\n  }\n}",
      "exampleOutput": ""
    },
    {
      "name": "Device Onboarding - Reset - Kentik - Example",
      "webName": "Device Onboarding - Reset - Kentik - Example",
      "assetType": "Workflow",
      "overview": "Automation removes flow test Cisco IOS device configuration and deletes device from Kentik to reset data for Device Onboarding - Kentik - Example automation",
      "iapVersions": [
        "2023.1"
      ],
      "exampleInput": "{\n  \"formData\":  {\n    \"deviceName\": \"ios_device\",\n    \"kentikAdapter\": \"Kentik v5\"\n  }\n}",
      "exampleOutput": ""
    }
  ],
  "relatedItems": {
    "adapters": [
      {
        "name": "adapter-kentik_v5",
        "webName": "",
        "overview": "Adapter that connects to Kentik",
        "isDependency": true,
        "repoLink": "https://gitlab.com/itentialopensource/adapters/observability/adapter-kentik_v5",
        "docLink": "",
        "webLink": "",
        "configurationNotes": "",
        "versions": [
          "^0.1.1"
        ]
      },
      {
        "name": "adapter-netbox_v33",
        "webName": "",
        "overview": "Adapter that connects to NetBox",
        "isDependency": true,
        "repoLink": "https://gitlab.com/itentialopensource/adapters/inventory/adapter-netbox_v33",
        "docLink": "https://docs.itential.com/opensource/docs/en/netbox-v33",
        "webLink": "",
        "configurationNotes": "",
        "versions": [
          "^2.0.0"
        ]
      },
      {
        "name": "adapter-service_now",
        "webName": "ServiceNow",
        "overview": "Adapter that connects to ServiceNow",
        "isDependency": true,
        "repoLink": "https://gitlab.com/itentialopensource/adapters/itsm-testing/adapter-servicenow",
        "docLink": "https://docs.itential.com/opensource/docs/en/servicenow",
        "webLink": "https://www.itential.com/adapters/servicenow/",
        "configurationNotes": "",
        "versions": [
          "^2.6.3"
        ]
      },
      {
        "name": "adapter-aws_ec2",
        "webName": "Amazon EC2",
        "overview": "Adapter that connects to AWS.",
        "isDependency": true,
        "repoLink": "https://gitlab.com/itentialopensource/adapters/cloud/adapter-aws_ec2",
        "docLink": "https://docs.itential.com/opensource/docs/amazon-aws-ec2",
        "webLink": "https://www.itential.com/adapters/amazon-ec2/",
        "configurationNotes": "",
        "versions": [
          "^0.6.9"
        ]
      },
      {
        "name": "adapter-automation_gateway",
        "webName": "Itential Automation Gateway",
        "overview": "Adapter that connects to Itential Automation Gateway",
        "isDependency": true,
        "repoLink": "",
        "docLink": "",
        "webLink": "https://www.itential.com/automation-gateway/",
        "configurationNotes": "",
        "versions": [
          "^4.29.0-2023.1.12"
        ]
      },
      {
        "name": "adapter-ms_teams",
        "webName": "Microsoft Teams",
        "overview": "Adapter that connects to Microsoft Teams",
        "isDependency": true,
        "repoLink": "https://gitlab.com/itentialopensource/adapters/notification-messaging/adapter-msteams",
        "docLink": "https://docs.itential.com/opensource/docs/microsoft-teams",
        "webLink": "https://www.itential.com/adapters/microsoft-teams/",
        "configurationNotes": "",
        "versions": [
          "^0.13.0"
        ]
      }
    ],
    "integrations": [],
    "ecosystemApplications": [],
    "workflowProjects": [
      {
        "name": "Kentik - REST",
        "webName": "Kentik - Device Management",
        "overview": "The integration of Itential and Kentik enables network teams to build automations that can immediately respond to events and alarms generated by the Kentik observability platform. In addition, Itential also offers a package of Pre-Built Automations to implement multiple use cases with the integration. The Kentik - REST Pre-Built provides automations that help Network Engineers automate common tasks performed in the Kentik platform. This Pre-Built contains multiple automations including:\\n\\n- Create a Device in Kentik - With this automation, users can create a device for a specific site and provide the device record as output.\\n- Run Device Flow Test - With this automation, users can run a device flow test for a specific device, providing the results of the test as output.",
        "isDependency": true,
        "repoLink": "https://gitlab.com/itentialopensource/pre-built-automations/kentik-rest",
        "docLink": "",
        "webLink": "https://www.itential.com/automations/kentik-device-management/",
        "versions": [
          "^1.0.3-2023.1.2"
        ]
      },
      {
        "name": "ServiceNow - Now Platform - REST",
        "webName": "ServiceNow Platform Modular Automations",
        "overview": "Project with workflows for the Now Platform from ServiceNow",
        "isDependency": true,
        "repoLink": "https://gitlab.com/itentialopensource/pre-built-automations/servicenow-now-platform-rest",
        "docLink": "",
        "webLink": "https://www.itential.com/automations/servicenow-platform-modular-automations/",
        "versions": [
          "^1.0.1-2023.1.2"
        ]
      },
      {
        "name": "Microsoft - Teams - REST",
        "webName": "Microsoft Teams Notification",
        "overview": "This Pre-Built integrates with the Microsoft Teams Open Source Adapter to send a notification within a Microsoft Teams channel.",
        "isDependency": true,
        "repoLink": "https://gitlab.com/itentialopensource/pre-built-automations/microsoft-teams-notification",
        "docLink": "",
        "webLink": "https://www.itential.com/automations/microsoft-teams-notification/",
        "versions": [
          "^1.0.0-2023.1.1"
        ]
      }
    ],
    "transformationProjects": [],
    "exampleProjects": []
  }
}