# Overview

This Pre-Built Automation bundle contains several example use cases that are applicable when Itential is integrated with the Kentik Platform. Because every environment is different, these use cases are fully functioning examples that can be easily modified to operate in your specific environment. These workflows have been written with modularity in mind to make them easy to understand and simple to modify to suite your needs. 

## Example Workflow Details
Example use case in this project include:

**AWS Blocked Traffic - Kentik - Example**
This is a workflow to receive an alert from Kentik for an AWS flow log, and determine whether to update an AWS Security Group based Source IP.

**AWS Blocked Traffic - Reset - Kentik - Example**
This is a workflow to reset the AWS Security Group for the AWS Blocked Traffic - Reset - Kentik - Example.

**Device Onboarding - Kentik - Example**
This is a workflow that will read inventory from an existing NetBox system, configure the device for monitoring, and onboard it into the Kentik platform.

**Device Onboarding - Reset - Kentik - Example**
This is a workflow to reset the Device Onboarding - Kentik - Example.

For further technical details on how to install and use this example project, please click the Technical Documentation tab.