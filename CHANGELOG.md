
## 1.1.4 [02-13-2024]

* Updates Cypress tests

See merge request itentialopensource/pre-built-automations/kentik-rest-example!23

---

## 1.1.3 [01-18-2024]

* Updates use case metadata value

See merge request itentialopensource/pre-built-automations/kentik-rest-example!22

---

## 1.1.2 [01-18-2024]

* Addresses compliance issues found in workflow tasks, documentation, and metadata.

See merge request itentialopensource/pre-built-automations/kentik-rest-example!20

---

## 1.1.1 [11-28-2023]

* Update documentation in pipeline

See merge request itentialopensource/pre-built-automations/kentik-rest-example!13

---

## 1.1.0 [11-28-2023]

* Adds project file and updates installation instructions

See merge request itentialopensource/pre-built-automations/kentik-rest-example!12

---

## 1.0.4 [11-10-2023]

* Updates webLink and demoLinks in metadata

See merge request itentialopensource/pre-built-automations/kentik-rest-example!11

---

## 1.0.3 [11-10-2023]

* Updates asset names and overview of Pre-Built

See merge request itentialopensource/pre-built-automations/kentik-rest-example!9

---

## 1.0.2 [11-08-2023]

* Updates device onboarding overview

See merge request itentialopensource/pre-built-automations/kentik-rest-example!7

---

## 1.0.1 [11-07-2023]

* Updates metadata

See merge request itentialopensource/pre-built-automations/staging/kentik-rest-example!6

---

## 1.0.0 [11-07-2023]

* Create 1.0.0 release

See merge request itentialopensource/pre-built-automations/staging/kentik-rest-example!5

---

## 0.0.7 [11-07-2023]

* Updates overview documentation for AWS Blocked Traffic and Device Onboarding automations

See merge request itentialopensource/pre-built-automations/staging/kentik-rest-example!4

---

## 0.0.6 [11-07-2023]

* Update AWS Blocked Traffic Event overview

See merge request itentialopensource/pre-built-automations/staging/kentik-rest-example!3

---

## 0.0.5 [11-06-2023]

* Updates Transformations, documentation, and inputs to AWS Blocked Traffic Event - Kentik - Example

See merge request itentialopensource/pre-built-automations/staging/kentik-rest-example!1

---

## 0.0.4 [11-02-2023]

* Updates Transformations, documentation, and inputs to AWS Blocked Traffic Event - Kentik - Example

See merge request itentialopensource/pre-built-automations/staging/kentik-rest-example!1

---

## 0.0.3 [11-01-2023]

* Updates 2023.1 template [skip ci]

See merge request itentialopensource/pre-built-automations/pre-built-template-2023-1!1

---

## 0.0.2 [11-01-2023]

* Updates 2023.1 template [skip ci]

See merge request itentialopensource/pre-built-automations/pre-built-template-2023-1!1

---
\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n
